class window.OrganismModel
  
  #cycle de virus de l'organisme en cours
  lifeCycle           : null
  # durée maximale du cycle de vie
  durationLifeCycle   : null
  # l'organisme est dans le noyau
  isOnCore            : false 
  # santé de l'organisme -> 100/100
  health              : 100
  # couche ou se situe actuellement l'organisme
  layer       : null
  # composition de rendu de l'écosysteme 
  stage       : null
  # critères des couches visuelles de l'écosysteme
  crust       : null
  # famille attribuée a l'écosystème
  family      : null 
  # génération de l'organisme
  generation  : null 

  # organisme vivant ou mort
  _isAlive     : false

  constructor: (stage, crust, family) ->
    @stage     = stage 
    @ctx       = stage.ctx
    @family    = family 
    @crust     = crust
    @lifeCycle = 0
    @layer     = 0
    @durationLifeCycle = 0

