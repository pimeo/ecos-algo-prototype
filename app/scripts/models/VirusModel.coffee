class window.VirusModel

  # identification unique de l'entité virus
  uid                       : null
  # tableau d'organismes
  _organisms                : null
  # durée maximale du cycle de vie du virus crée (sec)
  _durationLifeCycle        : null
  # limite de générations possible du virus crée
  _maxGenerations           : null
  # temps de regeneration du virus crée (sec)
  _timeBetweenEachGeneration: null
  # gestion du temps (sec)
  _timeClock                : null 
  # entité du virus vivant
  _isAlive                  : true
  # suspension du temps avant regénération d'un organisme
  _isTimePending            : false
  # entité du virus arrivée dans le noyau de l'écosystème
  _isOnCore                 : false
  # famille attribuée
  family                    : null
  # formation des couches selon la famille attribuée
  _crust                    : null
  # on force manuellement le virus a mourir
  _forceToDie               : false
  # canvas de rendu de l'ecosysteme
  stage                     : null

  # puissance attaque et période de cycle d'attaque de la famille
  _attack                   : {}
  # caractéristiques de déplacement du virus
  _movement                 : {} 

  constructor: (options) ->
    @uid         = new Date().getTime()
    @_organisms   = []
    @_timeClock   = 0
    @family       = options.family
    @_crust       = @family.crust
    @stage        = options.stage
    
    # si la creation d'un virus est régie sur des déclinaisons de propriétés de la famille attribuée
    # sinon nous prise en compte des propriétés par defaut de la famille attribuée
    @_durationLifeCycle   = if (options.durationLifeCycle) then options.durationLifeCycle else @family._durationLifeCycle 
    @_maxGenerations      = if (options.maxGenerations) then options.maxGenerations else @family._maxGenerations 
    @_timeBetweenEachGeneration = if (options.timeBetweenEachGeneration) then options.timeBetweenEachGeneration else @family._timeBetweenEachGeneration

    @_attack              = if (options.atack)    then options.attack else @family._attack
    @_movement            = if (options.movement) then  options.movement else window.Constants.VIRUS_MOVEMENT

  # fonction de mise a jour par rapport a l'horloge de l'écosysteme
  update: () ->
    @_timeClock++