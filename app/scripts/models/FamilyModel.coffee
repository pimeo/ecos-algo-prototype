class window.FamilyModel

  # type de famille
  _typeOfFamily           : null
  # durée maximale du cycle de vie d'un virus (sec)
  _durationLifeCycle      : null
  # temps de regeneration (sec)
  _timeBetweenEachGeneration : null
  # limite de générations possible
  _maxGenerations         : null
  # liste des familles
  _families               : null
  # propriétes des couches de la famille atribuée
  crust                   : null
  # couleur de la famille
  _color                   : null
  
  # puissance attaque et période de cycle d'attaque d'un virus
  _attack                  : {}

  constructor: (options) ->

    @_families = ["A", "B", "C"]

    @_typeOfFamily              = options.family
    @_durationLifeCycle         = options.durationLifeCycle
    @_maxGenerations            = options.maxGenerations
    @_timeBetweenEachGeneration = options.timeBetweenEachGeneration
    @_color                     = options.color

    @_attack                    = if options.attack then options.attack else window.Constants.VIRUS_ATTACK   
