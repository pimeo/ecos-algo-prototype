class window.ClockModel

  # ecart de temps entre chaque changement d'action
  _gap : null

  constructor: (options) ->
      @_gap = options.gap
      @_callback = if options.callback then options.callback else null