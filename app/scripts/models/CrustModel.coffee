class window.CrustModel
  
  # tableau des couches qui composent l'écosysteme
  _layers         : null
  # duree de traversée d'une couche
  _timeLayerCross : null

  # couche actuelle que traverse l'entité virus
  _layer          : null

  constructor: (options) ->
    @_layers = if options.layers then options.layers else window.Constants.CRUSTS
    @_timeLayerCross = if options.timeLayerCross then options.timeLayerCross else window.Constants.TIME_LAYERS_CROSS