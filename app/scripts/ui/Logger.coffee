class window.Logger

  _container       : null
  _contents        : null
  _button          : null
  _isPanelOpened   : true

  constructor: () ->
    @_container = $('.logger__container')
    @_button    = @_container.find('.logger--button')

    if @_isPanelOpened
      @_openPanelAction()
    else
      @_closePanelAction()

    @_button.on "click", @_toggleLoggerPanel

  _openVirusDetails: (e) =>
    e.preventDefault() if e?
    console.log "open"

  _openPanelAction : () ->
    @_container.addClass 'open'
    @_button.html "-"

  _closePanelAction : () ->
    @_container.removeClass 'open'
    @_button.html "+"

  _toggleLoggerPanel: (e) =>
    e.preventDefault() if e?
    if @_isPanelOpened
      @_openPanelAction()
    else
      @_closePanelAction()
    @_isPanelOpened = !@_isPanelOpened

  @display: (message) ->
    $('#logger').append(message + "\n")

  @info: (message) ->
    console.log message

  @updateEcosystemStatus: (viruses, families) ->
    # viruses
    template = "<tr><td>"+viruses.length+"</td><td>"+_.size(families)+"</td></tr>"
    $("#logger__resume--table tbody").html template

    window.Logger.updateVirusesStatus viruses

  @updateVirusesStatus: (viruses) ->
    for virus in viruses
      template = "<td class='virus--alive'><b class='virus--alive-status "
      template += (if virus._isAlive then " alive " else " dead ")
      template += "'></b></td>"
      template += "<td class='virus--name'>Virus "+virus.uid+"</td>"
      template += "<td class='virus--family'>"+virus.family._typeOfFamily+"</td>"
      template += "<td class='virus--lifecycle'>"+virus._durationLifeCycle+"</td>"
      template += "<td class='virus--generation'>"+virus._timeBetweenEachGeneration+"</td>"
      template += "<td class='virus--generation'>"+virus._maxGenerations+"</td>"
      currentVirusGeneration = if virus._organisms? then _.last(virus._organisms).generation else "DISPARU"
      template += "<td class='virus--current-generations'>"+currentVirusGeneration+"</td>"
      template += "<td class='virus--generation'>"
      if virus._organisms?
        template += "<table>"
        _.each(virus._organisms, (orga, index) =>
          #template += "here " + index + "<br/>"
          template += "<tr>"
          #template += "<td class='virus--alive'><b class='virus--alive-status "
          #template += (if orga? && orga._isAlive? then " alive " else " dead ")
          #template += "></b></td>"
          template += "<td>"+(orga.lifeCycle if orga?)+"/"+(orga.durationLifeCycle if orga?)+"</td>"
          template += "</tr>"
        )
        template += "</table>"
      "</td>"

      if $('#virus-'+virus.uid).length > 0
        $('#virus-'+virus.uid).html template
      else
        $('#logger__status--table tbody').append "<tr id='virus-"+virus.uid+"'>"+template+"</tr>"  

logger = null
$ () ->
  # logger
  logger = new window.Logger()
