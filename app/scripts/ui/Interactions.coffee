class window.Interactions

  _familiesChoices : ["A", "B", "C"]
  ecosystem  : null

  constructor: (ecosystem) ->
    @ecosystem = ecosystem
    @_initFamiliesFixtures()
    @_initVirusFixtures()
    @_initEvents()

  _initEvents: () ->
    $('.logger--add-virus-button').on 'click',  @_insertVirus
    $('#kill-virus-button').on 'click',         @_killVirus
    $('#kill-organism-button').on 'click',      @_killOrganism

  # inserer un virus dans l'écosysteme
  _insertVirus: (e) =>
    e.preventDefault() if e?
    for i in [0..100] by 1
      setTimeout (=>
        @ecosystem.addNewVirus
          familyName: "B"
      ), i*1000
   

  # detruire totalement un virus de l'écosysteme
  _killVirus: (e) =>
    e.preventDefault() if e?
    if $('#kill-virus-input').val() isnt ''
      @ecosystem.killVirus $('#kill-virus-input').val()

  # detruire l'organisme en cours de vie dans une entité virus
  _killOrganism: (e) =>
    e.preventDefault() if e?
    if $('#kill-virus-input').val() isnt ''
      @ecosystem.killOrganism $('#kill-virus-input').val()


  _initVirusFixtures: () ->
    # ajouter un virus dans l'écosysteme
    # on peut influencer sur les propriétés de la famille attribuée
    @ecosystem.addNewVirus
      familyName: "B"
      durationLifeCycle : 25
      #generations       : 8
      #timeBetweenEachGeneration : 3
      #attack :
      # kick : 15
      # time : 1 
      movement :
        angleOffset : 20
        speed       : 3 

    # setTimeout (=>
    #   @ecosystem.addNewVirus
    #     familyName: "A"
    #     durationLifeCycle : 100
    #     generations       : 3
    #     timeBetweenEachGeneration : 10
    # ), 1000

  # fixtures ajout familles dans l'écosysteme
  _initFamiliesFixtures: () ->

    ###
      3 Familles de Virus : A // B // C
    ###

    # Famille B
    @ecosystem.addNewFamily 
      family            : "B"
      durationLifeCycle : 15
      maxGenerations    : 5
      timeBetweenEachGeneration : 5
      color  : "#FF0000"
      attack :
        kick : 15
        time : 1 
      # couches
      crust :
        timeLayerCross: 2