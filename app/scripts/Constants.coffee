class window.Constants
  # nombre de couches qui composent la plateforme (int)
  @CRUSTS                   = 3
  # radius de la plateforme (px)
  @PLATFORM_CIRCLE_RADIUS   = 300
  # delai de passage pour une couche (sec)
  @TIME_LAYERS_CROSS        = 5

  # --- VIRUS

  # puissance d'attaque et période cycle d'attaque 
  @VIRUS_ATTACK =
    kick : 10 # 10 points par attaque (int)
    time : 2  # Période entre chaque attaque d'une seconde (sec)

  # déplacement d'un virus
  @VIRUS_MOVEMENT = 
    speed       : 1   # vitesse de déplacement d'un virus (int)
    angleOffset : 10  # degré de déplacement d'un virus par rapport a un autre virus lors d'une colision (deg)

  # --- CRITERES DE LIMITES MIN/MAX PENDANT UNE COLISION ENTRE VIRUS

  # contournement maximal d'un virus avant qu'il attaque (deg) 
  @LIMIT_OFFSET_ANGLE_AROUND_VIRUS =  180

  # temps maximal de contournement d'un virus avant qu'il attaque (sec)
  @LIMIT_TIME_AROUND_VIRUS         = 10
