# Organisme qui est appelé a naitre, a combattre et a mourir
class window.Organism extends OrganismModel

  vx : 0
  vy : 0

  constructor: (stage, crust, family) ->
    super stage, crust, family

  # donner la naissance à un organisme
  born: () ->
    @_isAlive = true
    window.Logger.display 'naissance organisme'

    # -- parametres extra noyau
    @_centerX   = @stage.width  /2
    @_centerY   = @stage.height /2
    @_radius    = @stage.radius 
    @_angle     = Math.random() * (Math.PI*2 - 0 + Math.PI*2)
    #@_speed     = @stage.radius / ((1000 / 16) * (@durationLifeCycle/ ((@crust._timeLayerCross * @crust._layers + 1) *2) ) )
    @_speed     = @stage.radius / ( window.Ecosystem.fps * @crust._timeLayerCross * @crust._layers)
    #@_speed     = @stage.radius / ((1000 / 30) *@durationLifeCycle)

    # -- parametres intro noyau
    @x   = parseInt(@stage.width/2)
    @y   = parseInt(@stage.height/2)

    # angle de déplacement aléatoire
    #@_angleCore   = Math.random() * (Math.PI *2) + 0
    @_angleCore   = Math.floor(Math.random() * 360)
    @_speedCore   = 3
    @_radiusCore  = 10

    @updateAngle()

  # donner la mort a l'organisme
  die: () ->
    @_isAlive = false
    window.Logger.display 'mort organisme'

  # boucle animation dans les couches du noyau de l'écosysteme
  render: () ->
    x = @_centerX + Math.cos(@_angle) * @_radius
    y = @_centerY + Math.sin(@_angle) * @_radius
    @ctx.beginPath()
    #@ctx.fillStyle = "rgba( 0, 0, 0, 1)"
    @ctx.fillStyle = @family._color
    @ctx.arc(x, y, 10, 0, Math.PI * 2, false )
    @ctx.fill()

    @_radius -= @_speed

  updateAngle: () ->
    rad = @_angleCore * Math.PI/ 180
    @vx = Math.floor(Math.cos(rad) * @_speedCore)
    @vy = Math.floor(Math.sin(rad) * @_speedCore)
  
  # boucle animation dans le noyau de l'écosysteme
  renderOnCore: () ->

    @ctx.beginPath()
    @ctx.fillStyle = @family._color
    
    # axis x constraints
    if ( @x >= (@stage.width - @_radiusCore) || @x <= @_radiusCore )
      # @_angleCore = Math.PI - @_angleCore
      @vx *= -1
    # axis y constraints
    if ( @y >= (@stage.height - @_radiusCore) || @y <= @_radiusCore )
      #@_angleCore = Math.PI*2 - @_angleCore
      @vy *= -1
      
    @x += @vx
    @y += @vy

    @ctx.arc(@x, @y, @_radiusCore, 0, Math.PI * 2, false)
    @ctx.fill()


