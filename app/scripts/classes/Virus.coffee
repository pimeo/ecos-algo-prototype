# création d'une entité virus qui va gérer les cycles de vies et mutations des organismes
class window.Virus extends VirusModel

  # generation en cours
  _generation : null

  constructor: (options) ->
    super options
    @_initializeVirus()
    
  _initializeVirus: () ->
    # première génération de virus
    @_generation = 0

    # lancement de la première génération
    @_createNewGenerationOfOrganism()
  
  # creation d'une nouvelle génération en fonction d'un interval
  _createNewGenerationOfOrganism: () =>
    @_organisms[@_generation] = new window.Organism(@stage, @_crust, @family)
    @_organisms[@_generation].generation = @_generation
    @_organisms[@_generation].durationLifeCycle = @_durationLifeCycle
    @_organisms[@_generation].born()
    #window.Logger.info 'creation nouvel organisme pour génération '+@_generation
    @_organisms[@_generation].layer = @_crust._layer
    # reset de la presence de l'organisme dans le noyau de l'écosystem
    @_organisms[@_generation].isOnCore  = false
    # reset du temps de cycle de vie
    #@_timeClock = 0

    # boucle de regénération d'un organisme selon le nombre de génération
    setTimeout(@_createNewGenerationOfOrganism, @_timeBetweenEachGeneration * 1000) if @_upgradeGeneration()

  _updateOrganisms: () ->

    if @_organisms?
      # on parcourt les organismes crées
      _.each(@_organisms, (organism, index) =>
        #console.log @_organisms
        if organism?
          # si l'organisme a dépasse son temps de cycle de vie
          if organism.lifeCycle >= @_durationLifeCycle
            organism.die()
            # on tue l'organisme
            @_killOrganism(index)
          else
            # si l'entité virus est forcé a mourir
            @_destroyVirus() if @_forceToDie

            # si nous changeons de couche
            if (organism.lifeCycle % @_crust._timeLayerCross == 0) && (organism.layer <= @_crust._layers)
              # nouvelle couche
              organism.layer++

              # l'organisme est arrivé dans le noyau
              if (organism.layer > @_crust._layers) && !organism.isOnCore
                #window.Logger.display '--- organisme arrivée dans le noyau
                organism.isOnCore = true
              #else
              #  window.Logger.display '--- changement de couche :: '+@_organisms[@_generation].layer 

            # incrementation de son cycle de vie
            organism.lifeCycle++ if organism?
            #window.Logger.display '++ cycle de vie'+@_organisms[@_generation].lifeCycle

        organism = null
      )

  # nouvelle generation
  _upgradeGeneration: () ->
    if @_generation >= (@_maxGenerations - 1)
      window.Logger.display 'nous avons atteins la fin de vie de ce virus'
      false
    else
      window.Logger.display 'nouvelle génération déployée'
      @_generation++
      true

  # destruction de l'entité virus + organismes
  _destroyVirus : () ->
    @_isAlive = false
    # suppression du tableau d'organismes
    @_organisms = null
    delete @_organisms

    #window.Logger.display "destruction du virus de l'ecosysteme"

  # meurtre forcé de l'entité virus
  killVirus: () ->
    @_forceToDie = true

  # tuer l'organisme de la generation en cours
  _killOrganism: (generationID) ->
    if @_organisms[generationID]?
      # destruction de l'organisme à la génération "generationID"
      #console.log "generation detruite ", generationID
      #@_organisms.splice(_.indexOf(@_organisms, generationID), 1) if _.indexOf(@_organisms, generationID) > -1
      @_organisms[generationID] = null
      # fin de vie de l'entité virus :: destruction de l'entité si le virus a atteint sa dernière génération
      @_destroyVirus() if !@_upgradeGeneration() && (generationID == (@_maxGenerations - 1) )

  # boucle horloge écosysteme
  update: () ->
    super
    if @_isAlive
      @_updateOrganisms()

  # boucle rendu ecosysteme
  render: () ->
    # pour chaque organisme crée
    _.each(@_organisms, (organism, index) =>
      if organism?
        # si l'organisme est dans le noyau de l'écosysteme
        if organism.isOnCore
          organism.renderOnCore()
        #sinon l'organisme parcourt les couches
        else
          organism.render()
    )





