class window.Platform
  stage   : null
  ctx     : null
  _crusts : 0
  _radius : 0
  _alpha  : 1
  _radiusCrust : 0
  _behavior    : null

  constructor: (behavior, options) ->
    @_behavior  = behavior
    @stage      = options
    @ctx        = options.ctx

    # autonomy
    if @_behavior == "autonomy"
      @_crusts    = options.crusts
      @_radius    = options.radius
      @_radiusCrust = @_radius/@_crusts
    
    # attacks
    else if @_behavior == "attacks"
      
    else
      console.log "no behavior"

  render: () ->
    @_renderAutonomy() if @_behavior == "autonomy"
    @_renderAttacks()  if @_behavior == "attacks"

  _renderAutonomy: () ->
    radius = @_radius
    alpha  = @_alpha 
    for i in [0...@_crusts] by 1
      @ctx.beginPath()
      @ctx.fillStyle = "rgba(15, 101, 161, "+alpha+")"
      @ctx.arc(@stage.width/2,@stage.height/2,radius,0,Math.PI*2, false); # outer (filled)
      @ctx.arc(@stage.width/2,@stage.height/2,radius-@_radiusCrust,0,Math.PI*2, true); # outer (unfills it)
      alpha   -= 0.2
      radius  -= @_radiusCrust
      @ctx.fill()

  _renderAttacks: () ->

