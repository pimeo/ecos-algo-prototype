class window.Family extends FamilyModel
  
  constructor: (options) ->
    super options
    # -- formation des couches 
    @crust = new window.Crust(
      # temps de propagation par couche
      timeLayerCross : options.crust.timeLayerCross
    )
    