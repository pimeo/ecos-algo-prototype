class window.Clock extends ClockModel
  
  # interval de temps
  @_interval : null

  constructor: (options) ->
    super options

  startEcosystemClock: () ->
    # interval with callback
    @_interval = window.setTimeout (=>
      @_callback()
      @startEcosystemClock()
    ), @_gap