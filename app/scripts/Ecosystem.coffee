###

  Description : Algorithme prototype d'un ecosysteme de virus
  Date        : 10/01/2014 
  Last Edited : 22/01/2014 
  Version     : 0.5.1
  Usage       : 
    - unité de temps de l'écosysteme  :: SECONDE
    - horloge de l'écosysteme         :: TOUTES LES 1 SECONDE

###
class window.Ecosystem

  _viruses      : null
  _families     : null
  _masterClock  : null
  _stage        : null
  _platform     : null

  saved         : false

  # recupération du nombre de frames par seconde pour obtenir la vitesse de déplacement
  # des virus
  @fps  : 30

  ###
  Comportements de l'écosysteme face a plusieurs situations
    "automony"  : les virus se créent / mutent et meurent à la fin de leur cycle de vie | aucune action entre virus
    "attacks"   : les virus se créent / se déplacent / mutent et attaques les autres virus
  ###
  _behaviors    : ["autonomy", "attacks"]

  # comportement choisi pour le fonctionnement de l'écosysteme
  _currentBehavior : null

  constructor: (behavior) ->
    @_viruses     = []
    @_families    = {}
    @_currentBehavior = behavior

    # canvas
    @_canvas  = document.getElementById "canvas"
    @_context = @_canvas.getContext "2d"
    _w = @_canvas.width  = window.innerWidth
    _h = @_canvas.height = window.innerHeight

    @_stage =
      canvas : @_canvas
      ctx    : @_context
      width  : _w
      height : _h
      crusts : window.Constants.CRUSTS
      radius : window.Constants.PLATFORM_CIRCLE_RADIUS

    # plateforme
    if _.indexOf(@_behaviors, @_currentBehavior) > -1
      @_platform = new window.Platform @_currentBehavior, @_stage
    else 
      throw new Error "Le comportement de l'écosysteme est incorrect ou manquant"

    # lancement boucle de rendu ecosysteme
    @_render()

    # -- horloge de l'écosysteme
    @_masterClock = new window.Clock(
      # setInterval en ms
      gap       : 1000
      callback  : @_update
    )
    # lancement de l'horloge de l'écosysteme
    @_masterClock.startEcosystemClock()

  addNewFamily: (options) ->
    family = new window.Family options
    if !@_families[options.family]
      @_families[options.family] = family

  addNewVirus: (options) ->
    if options.familyName?
      options.family = @_families[options.familyName]
      options.stage  = @_stage
      virus = new window.Virus options
      @_viruses.push virus

  # horloge globale : fréquence -> toutes les 1 seconde
  _update: () =>
    for virus in @_viruses
      if virus?
        if virus._isAlive
          virus.update()
        else
          @_viruses.splice _.indexOf(@_viruses, virus), 1
          virus = null
    window.Logger.updateEcosystemStatus @_viruses, @_families


  _render : () =>
    # control requestAnimationFrame via fps
    setTimeout (=>
     window.requestAnimationFrame @_render
    ), 1000 / window.Ecosystem.fps

    @_stage.ctx.clearRect(0, 0, @_stage.width, @_stage.height)
    @_platform.render() if @_platform?
    for virus in @_viruses
      if virus?
        if virus._isAlive
          virus.render()
    if !@saved
      @saved = !@saved
      setTimeout(@_showEcosystem, 3000)

  _showEcosystem: () =>
    console.log @

  # tuer une entité virus manuellement
  killVirus: (virus_id) ->
    for virus in @_viruses
      virus.killVirus() if virus.uid == parseInt(virus_id)

  # tuer l'organisme en cours de vie pour une entité virus
  killOrganism: (virus_id) ->
    for virus in @_viruses
      virus._killOrganism() if virus.uid == parseInt(virus_id) 

ecosystem = null
$ () ->

  # création de l'écosysteme
  ecosystem = new window.Ecosystem "autonomy"

  # creation outil d'interaction
  interactions = new window.Interactions(ecosystem)


